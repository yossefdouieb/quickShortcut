A keyboard sniffer that check if the keys combination that you typed is matching a shortcut and opens the matching application accordingly.

Screenshot:

![alt text](https://gitlab.com/yossefdouieb/quickShortcut/-/raw/master/demo.png)
