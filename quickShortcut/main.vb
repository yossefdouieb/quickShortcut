﻿Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.IO
Imports System.Reflection
Imports System.CodeDom
Imports System.CodeDom.Compiler
Imports Microsoft.VisualBasic

Public Class main

    'voice output
    Public SAPI

    'for the key-word list reading
    Dim dataSplited As Array
    Dim separator As String = "&"

    Dim needToClear As Boolean = False
    Dim safeProgramIsRunning As Boolean = False

    'string and thread for the backroundWorker 
    Public toSpeak As String = ""
    Public thread_speak As Thread

    Private Sub VizualSwitch11_CheckedChanged(sender As System.Object) Handles VizualSwitch11.CheckedChanged
        If Not VizualSwitch11.Checked Then
            Try
                thread_scan.Abort()
                NotifyIcon1.Icon = My.Resources.blue
                toSpeak = "Successfully Stoped!"
                If Not My.Settings.vocalCommands Then
                    MsgBox("Successfully Stoped!", MsgBoxStyle.Information)
                End If
                logBox.Text += vbNewLine + "Service Stoped..."
            Catch ex As Exception

            End Try
        Else

            thread_scan = New Thread(AddressOf Scan)
            thread_scan.IsBackground = True
            thread_scan.Start()
            hideAllForms()
            Me.WindowState = FormWindowState.Minimized
            NotifyIcon1.Visible = True
            NotifyIcon1.Icon = My.Resources.green
            NotifyIcon1.BalloonTipIcon = ToolTipIcon.Info
            toSpeak = "Successfully Started!"
            NotifyIcon1.BalloonTipTitle = "Successfully Started!"
            NotifyIcon1.BalloonTipText = "Double-Click To Reopen..."
            NotifyIcon1.ShowBalloonTip(2000)
            'Me.Hide()
            ShowInTaskbar = False

            'Updates Log TextBox
            logBox.Text += vbNewLine + "Service Started..."


        End If

        If My.Settings.vocalCommands Then
            Try
                BackgroundWorker1.RunWorkerAsync()
            Catch ex As Exception

            End Try
        End If
    End Sub

    Private Sub NotifyIcon1_MouseDoubleClick(sender As System.Object, e As System.Windows.Forms.MouseEventArgs) Handles NotifyIcon1.MouseDoubleClick
        'when double-click on the tray icon, opening the form again after minimizing to tray
        ShowInTaskbar = True
        Me.WindowState = FormWindowState.Normal
        NotifyIcon1.Visible = True
    End Sub


    Private Sub VizualSmallButton1_Click(sender As System.Object, e As System.EventArgs) Handles VizualSmallButton1.Click
        Settings.Show()
    End Sub

#Region "keylogger_code"
    Dim buffer As New List(Of String)
    Dim buffercat As String
    Dim stagingpoint As String
    Dim actual As String
    Dim initlog As Boolean = False
    Dim log As StreamWriter

    ' threading
    Public thread_scan As Thread
    Public thread_hide As Thread

    ' thread-safe calling for thread_hide
    Delegate Sub Change()

    ' getkey, API call to USER32.DLL
    <DllImport("USER32.DLL", EntryPoint:="GetAsyncKeyState", SetLastError:=True,
    CharSet:=CharSet.Unicode, ExactSpelling:=True,
    CallingConvention:=CallingConvention.StdCall)>
    Public Shared Function getkey(ByVal Vkey As Integer) As Boolean
    End Function

    Public Sub Scan()
        Dim foo As Integer
        While 1

            For foo = 1 To 93 Step 1
                If getkey(foo) Then
                    AddtoBuffer(foo, getkey(16))
                End If
            Next

            For foo = 186 To 192 Step 1
                If getkey(foo) Then
                    AddtoBuffer(foo, getkey(16))
                End If
            Next

            BufferToOutput()
            buffer.Clear()

            Thread.Sleep(120)
            SetText(stagingpoint)
        End While
    End Sub


    ' parses keycode and saves to buffer to be written
    Public Sub AddtoBuffer(ByVal foo As Integer, ByVal modifier As Boolean)
        If Not (foo = 1 Or foo = 2 Or foo = 8 Or foo = 9 Or foo = 13 Or (foo >= 17 And foo <= 20) Or foo = 27 Or (foo >= 32 And foo <= 40) Or (foo >= 44 And foo <= 57) Or (foo >= 65 And foo <= 93) Or (foo >= 186 And foo <= 192)) Then
            Exit Sub
        End If

        Select Case foo
            Case 48 To 57
                If modifier Then
                    Select Case foo
                        Case 48
                            actual = ")"
                        Case 49
                            actual = "!"
                        Case 50
                            actual = "@"
                        Case 51
                            actual = "#"
                        Case 52
                            actual = "$"
                        Case 53
                            actual = "%"
                        Case 54
                            actual = "^"
                        Case 55
                            actual = "&"
                        Case 56
                            actual = "*"
                        Case 57
                            actual = "("
                    End Select
                Else
                    actual = Convert.ToChar(foo)
                End If
            Case 65 To 90
                If modifier Then
                    actual = Convert.ToChar(foo)
                Else
                    actual = Convert.ToChar(foo + 32)
                End If
            Case 1
                'actual = "<LCLICK>"
                actual = ""
            Case 2
                actual = "<RCLICK>"
            Case 8
                actual = "<BACKSPACE>"
            Case 9
                actual = "<TAB>"
            Case 13
                actual = "<ENTER>"
            Case 17
                actual = "<CTRL>"
            Case 18
                actual = "<ALT>"
            Case 19
                actual = "<PAUSE>"
            Case 20
                actual = "<CAPSLOCK>"
            Case 27
                actual = "<ESC>"
            Case 32
                actual = " "
            Case 33
                actual = "<PAGEUP>"
            Case 34
                actual = "<PAGEDOWN>"
            Case 35
                actual = "<END>"
            Case 36
                actual = "<HOME>"
            Case 37
                actual = "<LEFT>"
            Case 38
                actual = "<UP>"
            Case 39
                actual = "<RIGHT>"
            Case 40
                actual = "<DOWN>"
            Case 44
                actual = "<PRNTSCRN>"
            Case 45
                actual = "<INSERT>"
            Case 46
                actual = "<DEL>"
            Case 47
                actual = "<HELP>"
            Case 186
                If modifier Then
                    actual = ":"
                Else
                    actual = ";"
                End If
                actual = ";"

            Case 187
                If modifier Then
                    actual = "+"
                Else
                    actual = "="
                End If
            Case 188
                If modifier Then
                    actual = "<"
                Else
                    actual = ","
                End If
            Case 189
                If modifier Then
                    actual = "_"
                Else
                    actual = "-"
                End If
            Case 190
                If modifier Then
                    actual = ">"
                Else
                    actual = "."
                End If
            Case 191
                If modifier Then
                    actual = "?"
                Else
                    actual = "/"
                End If
            Case 192
                If modifier Then
                    actual = "~"
                Else
                    actual = "`"
                End If
        End Select

        If buffer.Count <> 0 Then
            Dim bar As Integer = 0
            While bar < buffer.Count
                If buffer(bar) = actual Then
                    Exit Sub
                End If
                bar += 1
            End While
        End If

        needToClear = False
        buffer.Add(actual)
    End Sub

    ' writes buffer to output box
    Public Sub BufferToOutput()
        If buffer.Count <> 0 Then
            Dim qux As Integer = 0
            While qux < buffer.Count
                buffercat = buffercat & buffer(qux)
                qux += 1
            End While
            'SetText(txtOutput.Text & buffercat)
            stagingpoint = stagingpoint & buffercat
            buffercat = String.Empty
        End If
    End Sub

    Delegate Sub SetTextCallback(ByVal [text] As String)

    ' thread safe call to output text to output box
    Private Sub SetText(ByVal [text] As String)
        Try
            If txtOutput.InvokeRequired Then
                Dim d As New SetTextCallback(AddressOf SetText)
                Me.Invoke(d, New Object() {[text]})
            Else
                If Not needToClear Then
                    txtOutput.Text = [text]
                Else
                    txtOutput.Clear()
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub
#End Region

    Private Sub txtOutput_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtOutput.TextChanged
        dataSplited = My.Settings.keysData.Split(separator)

        'if txtOutput is not empty
        If Not txtOutput.Text = "" Then

            'iterate through the array and taking 2 step each time (0, 2, 4)...
            For x = 0 To dataSplited.Length - 1 Step 2

                'avoiding outOfRange exception
                If Not x >= dataSplited.Length - 1 Then
                    If dataSplited.Length > 1 Then

                        'if we have a match from the text inputed to the list of key-words
                        If txtOutput.Text.Contains(dataSplited(x)) Then
                            'checking that there is no safe program running at top-most
                            checkSafePrograms()
                            If Not safeProgramIsRunning Then
                                If Not dataSplited(x + 1).ToString.Contains("CODE") Then
                                    Try
                                        Process.Start(dataSplited(x + 1))
                                        txtOutput.Clear()
                                        txtOutput.Refresh()
                                        toSpeak = dataSplited(x) + " Entered Associated Task Opened"
                                        logBox.Text += vbNewLine + dataSplited(x) + " Entered " + dataSplited(x + 1) + " Opened"
                                        cleanBuffer()

                                        speakThreadHandler()

                                        Exit Sub
                                    Catch ex As Exception
                                        toSpeak = "Error Opening " + dataSplited(x + 1) + " See App Log"
                                        logBox.Text += vbNewLine + "Error Opening " + dataSplited(x + 1) + " Reason: " + ex.Message
                                        txtOutput.Clear()
                                        txtOutput.Refresh()
                                        needToClear = True
                                        cleanBuffer()

                                        speakThreadHandler()

                                        Exit Sub
                                    End Try
                                Else
                                    'MsgBox("CODE")
                                    executeVbCode(dataSplited(x + 1).ToString.Replace("CODE", ""))
                                    txtOutput.Clear()
                                    txtOutput.Refresh()
                                    toSpeak = dataSplited(x) + " Entered, Code Executed"
                                    logBox.Text += vbNewLine + dataSplited(x) + " Entered, Code Executed"
                                    cleanBuffer()

                                    speakThreadHandler()
                                End If
                            Else
                                toSpeak = dataSplited(x) + " Entered But A Safe Program Is Running..."
                                logBox.Text += vbNewLine + dataSplited(x) + " Entered But " + dataSplited(x + 1) + " Was Not Opened Because A Safe Program Is Running..."
                                txtOutput.Clear()
                                txtOutput.Refresh()
                                needToClear = True
                                cleanBuffer()

                                speakThreadHandler()

                                Exit Sub

                            End If
                        End If
                    End If
                Else
                    Exit For
                End If
            Next
        End If
    End Sub

    Public Sub checkSafePrograms()
        'reading the list of safe-programs
        Dim safePrograms As Array
        safePrograms = My.Settings.safeList.Split(separator)

        safeProgramIsRunning = False
        For Each item In safePrograms
            Try
                'if the windowOnTop function (that checks if a program is running at top-most) returns true, then we cant open the process (setting safeProgramIsRunning to True and exiting)...
                If windowOnTop(Process.GetProcessesByName(item.ToString.Replace(".exe", ""))(0).MainWindowHandle, item.ToString.Replace(".exe", "")) Then
                    safeProgramIsRunning = True
                    Exit For
                Else
                    'MsgBox(item.ToString.Replace(".exe", ""))
                End If
            Catch ex As Exception

            End Try
        Next
    End Sub

    'Declarations For The windowOnTop Function
    Private Const SW_SHOWMAXIMIZED As Integer = 3
    Private Const SW_SHOWMINIMIZED As Integer = 2
    Private Const SW_SHOWNORMAL As Integer = 1

    Private Structure POINTAPI
        Public x As Integer
        Public y As Integer
    End Structure

    Private Structure RECT
        Public Left As Integer
        Public Top As Integer
        Public Right As Integer
        Public Bottom As Integer
    End Structure

    Private Structure WINDOWPLACEMENT
        Public Length As Integer
        Public flags As Integer
        Public showCmd As Integer
        Public ptMinPosition As POINTAPI
        Public ptMaxPosition As POINTAPI
        Public rcNormalPosition As RECT
    End Structure

    Private Declare Function GetWindowPlacement Lib "user32" (ByVal hwnd As IntPtr, ByRef lpwndpl As WINDOWPLACEMENT) As Integer
    Private Declare Function GetForegroundWindow Lib "user32" Alias "GetForegroundWindow" () As IntPtr


    Public Function windowOnTop(Handle As IntPtr, processName As String) As Boolean
        Dim wp As WINDOWPLACEMENT
        wp.Length = System.Runtime.InteropServices.Marshal.SizeOf(wp)
        GetWindowPlacement(Handle, wp)
        If wp.showCmd = SW_SHOWMAXIMIZED Then ' is window maximized?
            If Handle = GetForegroundWindow Then ' is the window foreground?
                'TextBox1.Text += "Maximized and forground" + vbNewLine
                Return True
            Else
                'TextBox1.Text += "Maximized" + vbNewLine
                Return False
            End If
        ElseIf wp.showCmd = SW_SHOWNORMAL Then
            If Handle = GetForegroundWindow Then
                'TextBox1.Text += "Normal size and forground" + vbNewLine
                Return True
            Else
                'TextBox1.Text += "Normal" + vbNewLine
                Return False
            End If
        ElseIf wp.showCmd = SW_SHOWMINIMIZED Then
            'TextBox1.Text += "Window is Minimized" + vbNewLine
            Return False
        Else
            'TextBox1.Text += "NoState" + vbNewLine
            If Process.GetProcessesByName(processName).Count > 0 Then
                Return True
            Else
                Return False
            End If
        End If
    End Function

    Public Sub cleanBuffer()
        'clean to avoid endless loop
        buffer.Clear()
        buffercat = ""
        stagingpoint = ""
        actual = ""
        initlog = False
    End Sub

    Private Sub main_Resize(sender As System.Object, e As System.EventArgs) Handles MyBase.Resize
        'if the program is activated instead a normal minimize - minimize to tray
        If Me.WindowState = FormWindowState.Minimized And VizualSwitch11.Checked Then
            NotifyIcon1.Visible = True
            NotifyIcon1.BalloonTipIcon = ToolTipIcon.Info
            'NotifyIcon1.BalloonTipTitle = "Successfully Started!"
            'NotifyIcon1.BalloonTipText = "Double-Click To Reopen..."
            'NotifyIcon1.ShowBalloonTip(2000)
            'Me.Hide()
            ShowInTaskbar = False
        End If
    End Sub

    Private Sub main_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        NotifyIcon1.Visible = True
        NotifyIcon1.Icon = My.Resources.blue
        'text to speak engine
        SAPI = CreateObject("SAPI.spvoice")
        thread_speak = New Thread(AddressOf speak)
        thread_speak.IsBackground = True

        'if the user chose to run the program at windows startup - automatically minimize to Tray 
        If My.Settings.startupEnabled Then
            VizualSwitch11.Checked = True
            Me.WindowState = FormWindowState.Minimized
            NotifyIcon1.Visible = True
            NotifyIcon1.BalloonTipIcon = ToolTipIcon.Info
            NotifyIcon1.Icon = My.Resources.green
            NotifyIcon1.BalloonTipTitle = "Successfully Started!"
            NotifyIcon1.BalloonTipText = "Double-Click To Reopen..."
            NotifyIcon1.ShowBalloonTip(2000)
            'Me.Hide()
            ShowInTaskbar = False

            'Updates Log TextBox
            logBox.Text += vbNewLine + "Service Started..."

            'start scanning for user input
            thread_scan = New Thread(AddressOf Scan)
            thread_scan.IsBackground = True
            thread_scan.Start()
        Else

        End If
    End Sub

    Private Sub BackgroundWorker1_DoWork(sender As System.Object, e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork
        speak()
    End Sub

    Public Sub speak()
        SAPI.speak(toSpeak)
    End Sub

    Private Sub ExitToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ExitToolStripMenuItem.Click
        Application.Exit()
    End Sub

    Public Sub speakThreadHandler()
        If My.Settings.vocalCommands Then
            If BackgroundWorker1.IsBusy Then
                Try
                    thread_speak = New Thread(AddressOf speak)
                    thread_speak.IsBackground = True
                    thread_speak.Start()
                Catch ex As Exception

                End Try
            Else
                BackgroundWorker1.RunWorkerAsync()
            End If
        End If
    End Sub

    Private Sub PauseToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles PauseToolStripMenuItem.Click
        Try
            If PauseToolStripMenuItem.Text = "Pause" Then
                thread_scan.Abort()

                logBox.Text += vbNewLine + "Service Paused..."
                NotifyIcon1.Icon = My.Resources.yellow
                PauseToolStripMenuItem.Text = "Resume"
                pauseLabel.Text = "Paused"
                toSpeak = "application paused"
                speakThreadHandler()
            Else
                thread_scan = New Thread(AddressOf Scan)
                thread_scan.IsBackground = True
                thread_scan.Start()

                logBox.Text += vbNewLine + "Service Resumed..."
                NotifyIcon1.Icon = My.Resources.green
                PauseToolStripMenuItem.Text = "Pause"
                pauseLabel.Text = ""
                toSpeak = "application resumed"
                speakThreadHandler()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub main_FormClosing(sender As System.Object, e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        NotifyIcon1.Visible = False
    End Sub

    Public Sub executeVbCode(ByVal input As String)
        ' Read code from file  
        'My.Computer.FileSystem.ReadAllText("Code.txt")

        ' Create "code" literal to pass to the compiler.  
        '  
        ' Notice the <% = input % > where the code read from the text file (Code.txt)   
        ' is inserted into the code fragment.  
        Dim code = <code>  
                       Imports System  
                       Imports System.Windows.Forms  
  
                       Public Class TempClass  
                           Public Sub UpdateText(ByVal txtOutput As TextBox)  
                               <%= input %>  
                           End Sub  
                       End Class  
                   </code>

        ' Create the VB.NET compiler.  
        Dim vbProv = New VBCodeProvider()
        ' Create parameters to pass to the compiler.  
        Dim vbParams = New CompilerParameters()
        ' Add referenced assemblies.  
        vbParams.ReferencedAssemblies.Add("mscorlib.dll")
        vbParams.ReferencedAssemblies.Add("System.dll")
        vbParams.ReferencedAssemblies.Add("System.Windows.Forms.dll")
        vbParams.GenerateExecutable = False
        ' Ensure we generate an assembly in memory and not as a physical file.  
        vbParams.GenerateInMemory = True

        ' Compile the code and get the compiler results (contains errors, etc.)  
        Dim compResults = vbProv.CompileAssemblyFromSource(vbParams, code.Value)

        ' Check for compile errors  
        If compResults.Errors.Count > 0 Then

            ' Show each error.  
            For Each er In compResults.Errors
                MessageBox.Show(er.ToString())
            Next

        Else

            ' Create instance of the temporary compiled class.  
            Dim obj As Object = compResults.CompiledAssembly.CreateInstance("TempClass")
            ' An array of object that represent the arguments to be passed to our method (UpdateText).  
            Dim args() As Object = {Me.logBox}
            ' Execute the method by passing the method name and arguments.  
            Try
                Dim t As Type = obj.GetType().InvokeMember("UpdateText", BindingFlags.InvokeMethod, Nothing, obj, args)
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try

        End If

    End Sub

    Public Sub hideAllForms()
        codePad.Hide()
        keysList.Hide()
        Process_Dialog.Hide()
        safeList.Hide()
        Settings.Hide()
    End Sub

End Class
