﻿Imports System.IO
Imports System.Reflection
Imports System.CodeDom
Imports System.CodeDom.Compiler
Imports Microsoft.VisualBasic

Public Class codePad

    Private Sub VizualSmallButton1_Click(sender As System.Object, e As System.EventArgs) Handles VizualSmallButton1.Click
        For Each row As DataGridViewRow In keysList.DataGridView1.Rows
            If row.Cells(1).Value = "CODE" Then
                row.Cells(1).Value = "CODE" + RichTextBox1.Text
                Me.Hide()
            End If
        Next
    End Sub

    Public Sub executeVbCode(ByVal input As String)
        ' Read code from file  
        'My.Computer.FileSystem.ReadAllText("Code.txt")

        ' Create "code" literal to pass to the compiler.  
        '  
        ' Notice the <% = input % > where the code read from the text file (Code.txt)   
        ' is inserted into the code fragment.  
        Dim code = <code>  
                       Imports System  
                       Imports System.Windows.Forms  
  
                       Public Class TempClass  
                           Public Sub UpdateText(ByVal txtOutput As TextBox)  
                               <%= input %>  
                           End Sub  
                       End Class  
                   </code>

        ' Create the VB.NET compiler.  
        Dim vbProv = New VBCodeProvider()
        ' Create parameters to pass to the compiler.  
        Dim vbParams = New CompilerParameters()
        ' Add referenced assemblies.  
        vbParams.ReferencedAssemblies.Add("mscorlib.dll")
        vbParams.ReferencedAssemblies.Add("System.dll")
        vbParams.ReferencedAssemblies.Add("System.Windows.Forms.dll")
        vbParams.GenerateExecutable = False
        ' Ensure we generate an assembly in memory and not as a physical file.  
        vbParams.GenerateInMemory = True

        ' Compile the code and get the compiler results (contains errors, etc.)  
        Dim compResults = vbProv.CompileAssemblyFromSource(vbParams, code.Value)

        ' Check for compile errors  
        If compResults.Errors.Count > 0 Then

            ' Show each error.  
            For Each er In compResults.Errors
                MessageBox.Show(er.ToString())
            Next

        Else

            ' Create instance of the temporary compiled class.  
            Dim obj As Object = compResults.CompiledAssembly.CreateInstance("TempClass")
            ' An array of object that represent the arguments to be passed to our method (UpdateText).  
            Dim args() As Object = {Me.txtOutput}
            ' Execute the method by passing the method name and arguments.  
            Try
                Dim t As Type = obj.GetType().InvokeMember("UpdateText", BindingFlags.InvokeMethod, Nothing, obj, args)
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try

        End If

    End Sub

    Private Sub VizualSmallButton2_Click(sender As System.Object, e As System.EventArgs) Handles VizualSmallButton2.Click
        If VizualCombo1.SelectedIndex = 0 Then
            executeVbCode(RichTextBox1.Text)
        End If
    End Sub

    Private Sub VizualSmallButton3_Click(sender As System.Object, e As System.EventArgs) Handles VizualSmallButton3.Click
        If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            RichTextBox1.Text = My.Computer.FileSystem.ReadAllText(OpenFileDialog1.FileName)
        End If
    End Sub

    Private Sub codePad_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        VizualCombo1.SelectedIndex = 0
    End Sub
End Class