﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Settings
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.VizualSmallButton1 = New quickShortcut.VizualSmallButton()
        Me.VizualSmallButton2 = New quickShortcut.VizualSmallButton()
        Me.VizualCheckbox1 = New quickShortcut.VizualCheckbox()
        Me.VizualCheckbox2 = New quickShortcut.VizualCheckbox()
        Me.SuspendLayout()
        '
        'VizualSmallButton1
        '
        Me.VizualSmallButton1.Colors = New quickShortcut.Bloom(-1) {}
        Me.VizualSmallButton1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.VizualSmallButton1.Customization = ""
        Me.VizualSmallButton1.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Bold)
        Me.VizualSmallButton1.Image = Nothing
        Me.VizualSmallButton1.Location = New System.Drawing.Point(12, 12)
        Me.VizualSmallButton1.Name = "VizualSmallButton1"
        Me.VizualSmallButton1.NoRounding = False
        Me.VizualSmallButton1.Size = New System.Drawing.Size(176, 30)
        Me.VizualSmallButton1.TabIndex = 0
        Me.VizualSmallButton1.Text = "Key-Words Settings"
        Me.VizualSmallButton1.Transparent = False
        '
        'VizualSmallButton2
        '
        Me.VizualSmallButton2.Colors = New quickShortcut.Bloom(-1) {}
        Me.VizualSmallButton2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.VizualSmallButton2.Customization = ""
        Me.VizualSmallButton2.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Bold)
        Me.VizualSmallButton2.Image = Nothing
        Me.VizualSmallButton2.Location = New System.Drawing.Point(12, 48)
        Me.VizualSmallButton2.Name = "VizualSmallButton2"
        Me.VizualSmallButton2.NoRounding = False
        Me.VizualSmallButton2.Size = New System.Drawing.Size(176, 30)
        Me.VizualSmallButton2.TabIndex = 1
        Me.VizualSmallButton2.Text = "Safe Programs Settings"
        Me.VizualSmallButton2.Transparent = False
        '
        'VizualCheckbox1
        '
        Me.VizualCheckbox1.Checked = False
        Me.VizualCheckbox1.Colors = New quickShortcut.Bloom(-1) {}
        Me.VizualCheckbox1.Customization = ""
        Me.VizualCheckbox1.Font = New System.Drawing.Font("Verdana", 8.0!)
        Me.VizualCheckbox1.Image = Nothing
        Me.VizualCheckbox1.Location = New System.Drawing.Point(12, 84)
        Me.VizualCheckbox1.Name = "VizualCheckbox1"
        Me.VizualCheckbox1.NoRounding = False
        Me.VizualCheckbox1.Size = New System.Drawing.Size(168, 20)
        Me.VizualCheckbox1.TabIndex = 2
        Me.VizualCheckbox1.Text = "Run At Startup"
        Me.VizualCheckbox1.Transparent = False
        '
        'VizualCheckbox2
        '
        Me.VizualCheckbox2.Checked = False
        Me.VizualCheckbox2.Colors = New quickShortcut.Bloom(-1) {}
        Me.VizualCheckbox2.Customization = ""
        Me.VizualCheckbox2.Font = New System.Drawing.Font("Verdana", 8.0!)
        Me.VizualCheckbox2.Image = Nothing
        Me.VizualCheckbox2.Location = New System.Drawing.Point(12, 110)
        Me.VizualCheckbox2.Name = "VizualCheckbox2"
        Me.VizualCheckbox2.NoRounding = False
        Me.VizualCheckbox2.Size = New System.Drawing.Size(168, 20)
        Me.VizualCheckbox2.TabIndex = 3
        Me.VizualCheckbox2.Text = "Enable Vocal Output"
        Me.VizualCheckbox2.Transparent = False
        '
        'Settings
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(200, 261)
        Me.Controls.Add(Me.VizualCheckbox2)
        Me.Controls.Add(Me.VizualCheckbox1)
        Me.Controls.Add(Me.VizualSmallButton2)
        Me.Controls.Add(Me.VizualSmallButton1)
        Me.Name = "Settings"
        Me.Text = "Settings"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents VizualSmallButton1 As quickShortcut.VizualSmallButton
    Friend WithEvents VizualSmallButton2 As quickShortcut.VizualSmallButton
    Friend WithEvents VizualCheckbox1 As quickShortcut.VizualCheckbox
    Friend WithEvents VizualCheckbox2 As quickShortcut.VizualCheckbox
End Class
