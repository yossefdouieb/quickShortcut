﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class keysList
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.VizualSmallButton2 = New quickShortcut.VizualSmallButton()
        Me.VizualSmallButton1 = New quickShortcut.VizualSmallButton()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataGridView1
        '
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1, Me.Column2})
        Me.DataGridView1.Location = New System.Drawing.Point(12, 12)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(293, 238)
        Me.DataGridView1.TabIndex = 2
        '
        'Column1
        '
        Me.Column1.HeaderText = "Shortcut Key"
        Me.Column1.Name = "Column1"
        '
        'Column2
        '
        Me.Column2.HeaderText = "Open"
        Me.Column2.Name = "Column2"
        Me.Column2.Width = 150
        '
        'VizualSmallButton2
        '
        Me.VizualSmallButton2.Colors = New quickShortcut.Bloom(-1) {}
        Me.VizualSmallButton2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.VizualSmallButton2.Customization = ""
        Me.VizualSmallButton2.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Bold)
        Me.VizualSmallButton2.Image = Nothing
        Me.VizualSmallButton2.Location = New System.Drawing.Point(12, 256)
        Me.VizualSmallButton2.Name = "VizualSmallButton2"
        Me.VizualSmallButton2.NoRounding = False
        Me.VizualSmallButton2.Size = New System.Drawing.Size(67, 30)
        Me.VizualSmallButton2.TabIndex = 3
        Me.VizualSmallButton2.Text = "Clear"
        Me.VizualSmallButton2.Transparent = False
        '
        'VizualSmallButton1
        '
        Me.VizualSmallButton1.Colors = New quickShortcut.Bloom(-1) {}
        Me.VizualSmallButton1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.VizualSmallButton1.Customization = ""
        Me.VizualSmallButton1.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Bold)
        Me.VizualSmallButton1.Image = Nothing
        Me.VizualSmallButton1.Location = New System.Drawing.Point(238, 256)
        Me.VizualSmallButton1.Name = "VizualSmallButton1"
        Me.VizualSmallButton1.NoRounding = False
        Me.VizualSmallButton1.Size = New System.Drawing.Size(67, 30)
        Me.VizualSmallButton1.TabIndex = 1
        Me.VizualSmallButton1.Text = "Save"
        Me.VizualSmallButton1.Transparent = False
        '
        'keysList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(317, 295)
        Me.Controls.Add(Me.VizualSmallButton2)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.VizualSmallButton1)
        Me.Name = "keysList"
        Me.Text = "Key-Words Settings"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents VizualSmallButton1 As quickShortcut.VizualSmallButton
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents VizualSmallButton2 As quickShortcut.VizualSmallButton
End Class
