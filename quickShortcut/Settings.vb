﻿Imports System.Threading

Public Class Settings

    Private Sub VizualSmallButton1_Click(sender As System.Object, e As System.EventArgs) Handles VizualSmallButton1.Click
        keysList.Show()
        Me.Hide()
    End Sub

    Private Sub VizualSmallButton2_Click(sender As System.Object, e As System.EventArgs) Handles VizualSmallButton2.Click
        safeList.Show()
        Me.Hide()
    End Sub

    Private Sub VizualCheckbox1_CheckedChanged(sender As System.Object) Handles VizualCheckbox1.CheckedChanged
        'Adding a Key To The Registry To Run At Windows Startup
        If Not VizualCheckbox1.Checked Then
            Try
                My.Computer.Registry.LocalMachine.OpenSubKey("SOFTWARE\Microsoft\Windows\CurrentVersion\Run", True).DeleteValue(Application.ProductName)
                My.Settings.startupEnabled = False
                My.Settings.Save()
                My.Settings.Upgrade()

                main.toSpeak = "Removed From Windows Startup!"

                If Not My.Settings.vocalCommands Then
                    MsgBox("Removed From Windows Startup!", MsgBoxStyle.Information)
                End If
            Catch ex As Exception
                MsgBox("Error!, Reason: " + ex.Message, MsgBoxStyle.Exclamation)
            End Try
        Else
            'remove the key from the registry
            Try
                My.Computer.Registry.LocalMachine.OpenSubKey("SOFTWARE\Microsoft\Windows\CurrentVersion\Run", True).SetValue(Application.ProductName, Application.ExecutablePath)
                My.Settings.startupEnabled = True
                My.Settings.Save()
                My.Settings.Upgrade()

                main.toSpeak = "Added To Windows Startup!"
                If Not My.Settings.vocalCommands Then
                    MsgBox("Added To Windows Startup!", MsgBoxStyle.Information)
                End If
            Catch ex As Exception
                MsgBox("Error!, Reason: " + ex.Message, MsgBoxStyle.Exclamation)
            End Try
        End If

        'if the vocal output is enabled - speak instead from msgbox
        If My.Settings.vocalCommands Then
            If main.BackgroundWorker1.IsBusy Then
                Try
                    main.thread_speak = New Thread(AddressOf main.speak)
                    main.thread_speak.IsBackground = True
                    main.thread_speak.Start()
                Catch ex As Exception

                End Try
            Else
                main.BackgroundWorker1.RunWorkerAsync()
            End If
        End If
    End Sub

    Private Sub VizualCheckbox2_CheckedChanged(sender As System.Object) Handles VizualCheckbox2.CheckedChanged
        'enabling/disabling vocal output (simple)
        If Not VizualCheckbox2.Checked Then
            My.Settings.vocalCommands = False
            My.Settings.Save()
            My.Settings.Upgrade()
            main.toSpeak = "Vocal Output Disabled!"
        Else
            My.Settings.vocalCommands = True
            My.Settings.Save()
            My.Settings.Upgrade()
            main.toSpeak = "Vocal Output Enabled!"
        End If

        'if the BackgroundWorker is busy (in middle of Talking) - start a new thread to speak straight after him
        If main.BackgroundWorker1.IsBusy Then
            Try
                main.thread_speak = New Thread(AddressOf main.speak)
                main.thread_speak.IsBackground = True
                main.thread_speak.Start()
            Catch ex As Exception

            End Try
        Else
            main.BackgroundWorker1.RunWorkerAsync()
        End If

    End Sub

    Private Sub Settings_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        'read the app settings, and check the checkboxes if the settings are enabled
        If My.Settings.vocalCommands Then
            VizualCheckbox2.Checked = True
            VizualCheckbox2.Refresh()
        End If

        If My.Settings.startupEnabled Then
            VizualCheckbox1.Checked = True
            VizualCheckbox1.Refresh()
        End If

    End Sub
End Class