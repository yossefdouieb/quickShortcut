﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class main
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(main))
        Me.txtOutput = New System.Windows.Forms.RichTextBox()
        Me.NotifyIcon1 = New System.Windows.Forms.NotifyIcon(Me.components)
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.PauseToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.BackgroundWorker1 = New System.ComponentModel.BackgroundWorker()
        Me.VizualSmallButton1 = New quickShortcut.VizualSmallButton()
        Me.VizualSwitch11 = New quickShortcut.VizualSwitch1()
        Me.pauseLabel = New System.Windows.Forms.Label()
        Me.logBox = New System.Windows.Forms.TextBox()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtOutput
        '
        Me.txtOutput.Location = New System.Drawing.Point(95, 148)
        Me.txtOutput.Name = "txtOutput"
        Me.txtOutput.Size = New System.Drawing.Size(148, 47)
        Me.txtOutput.TabIndex = 3
        Me.txtOutput.Text = ""
        '
        'NotifyIcon1
        '
        Me.NotifyIcon1.ContextMenuStrip = Me.ContextMenuStrip1
        Me.NotifyIcon1.Icon = CType(resources.GetObject("NotifyIcon1.Icon"), System.Drawing.Icon)
        Me.NotifyIcon1.Text = "Quick Shortcut"
        Me.NotifyIcon1.Visible = True
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PauseToolStripMenuItem, Me.ExitToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(106, 48)
        '
        'PauseToolStripMenuItem
        '
        Me.PauseToolStripMenuItem.Name = "PauseToolStripMenuItem"
        Me.PauseToolStripMenuItem.Size = New System.Drawing.Size(105, 22)
        Me.PauseToolStripMenuItem.Text = "Pause"
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(105, 22)
        Me.ExitToolStripMenuItem.Text = "Exit"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(10, 62)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(28, 13)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "Log:"
        '
        'BackgroundWorker1
        '
        Me.BackgroundWorker1.WorkerSupportsCancellation = True
        '
        'VizualSmallButton1
        '
        Me.VizualSmallButton1.Colors = New quickShortcut.Bloom(-1) {}
        Me.VizualSmallButton1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.VizualSmallButton1.Customization = ""
        Me.VizualSmallButton1.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Bold)
        Me.VizualSmallButton1.Image = Nothing
        Me.VizualSmallButton1.Location = New System.Drawing.Point(253, 12)
        Me.VizualSmallButton1.Name = "VizualSmallButton1"
        Me.VizualSmallButton1.NoRounding = False
        Me.VizualSmallButton1.Size = New System.Drawing.Size(80, 30)
        Me.VizualSmallButton1.TabIndex = 4
        Me.VizualSmallButton1.Text = "Settings"
        Me.VizualSmallButton1.Transparent = False
        '
        'VizualSwitch11
        '
        Me.VizualSwitch11.Checked = False
        Me.VizualSwitch11.Colors = New quickShortcut.Bloom(-1) {}
        Me.VizualSwitch11.Customization = ""
        Me.VizualSwitch11.Font = New System.Drawing.Font("Verdana", 8.0!)
        Me.VizualSwitch11.Image = Nothing
        Me.VizualSwitch11.Location = New System.Drawing.Point(12, 12)
        Me.VizualSwitch11.Name = "VizualSwitch11"
        Me.VizualSwitch11.NoRounding = False
        Me.VizualSwitch11.Size = New System.Drawing.Size(70, 30)
        Me.VizualSwitch11.TabIndex = 2
        Me.VizualSwitch11.Text = "VizualSwitch11"
        Me.VizualSwitch11.Transparent = False
        '
        'pauseLabel
        '
        Me.pauseLabel.AutoSize = True
        Me.pauseLabel.Location = New System.Drawing.Point(92, 22)
        Me.pauseLabel.Name = "pauseLabel"
        Me.pauseLabel.Size = New System.Drawing.Size(0, 13)
        Me.pauseLabel.TabIndex = 7
        '
        'logBox
        '
        Me.logBox.Location = New System.Drawing.Point(12, 79)
        Me.logBox.Multiline = True
        Me.logBox.Name = "logBox"
        Me.logBox.Size = New System.Drawing.Size(321, 188)
        Me.logBox.TabIndex = 8
        '
        'main
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(345, 279)
        Me.Controls.Add(Me.logBox)
        Me.Controls.Add(Me.pauseLabel)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.VizualSmallButton1)
        Me.Controls.Add(Me.VizualSwitch11)
        Me.Controls.Add(Me.txtOutput)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "main"
        Me.Text = "Quick Shortcut"
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents VizualSwitch11 As quickShortcut.VizualSwitch1
    Friend WithEvents txtOutput As System.Windows.Forms.RichTextBox
    Friend WithEvents NotifyIcon1 As System.Windows.Forms.NotifyIcon
    Friend WithEvents VizualSmallButton1 As quickShortcut.VizualSmallButton
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents BackgroundWorker1 As System.ComponentModel.BackgroundWorker
    Friend WithEvents ContextMenuStrip1 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents ExitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PauseToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents pauseLabel As System.Windows.Forms.Label
    Friend WithEvents logBox As System.Windows.Forms.TextBox

End Class
