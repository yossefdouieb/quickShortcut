﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class safeList
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ListBox1 = New System.Windows.Forms.ListBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.VizualSmallButton1 = New quickShortcut.VizualSmallButton()
        Me.VizualSmallButton2 = New quickShortcut.VizualSmallButton()
        Me.VizualSmallButton3 = New quickShortcut.VizualSmallButton()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(10, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(345, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "The Program Will Not Be Active While You Writing On Those Programs:"
        '
        'ListBox1
        '
        Me.ListBox1.FormattingEnabled = True
        Me.ListBox1.Location = New System.Drawing.Point(13, 25)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(342, 186)
        Me.ListBox1.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 214)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(182, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "*Double-Click To Remove A Program"
        '
        'VizualSmallButton1
        '
        Me.VizualSmallButton1.Colors = New quickShortcut.Bloom(-1) {}
        Me.VizualSmallButton1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.VizualSmallButton1.Customization = ""
        Me.VizualSmallButton1.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Bold)
        Me.VizualSmallButton1.Image = Nothing
        Me.VizualSmallButton1.Location = New System.Drawing.Point(295, 234)
        Me.VizualSmallButton1.Name = "VizualSmallButton1"
        Me.VizualSmallButton1.NoRounding = False
        Me.VizualSmallButton1.Size = New System.Drawing.Size(60, 30)
        Me.VizualSmallButton1.TabIndex = 3
        Me.VizualSmallButton1.Text = "Save"
        Me.VizualSmallButton1.Transparent = False
        '
        'VizualSmallButton2
        '
        Me.VizualSmallButton2.Colors = New quickShortcut.Bloom(-1) {}
        Me.VizualSmallButton2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.VizualSmallButton2.Customization = ""
        Me.VizualSmallButton2.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Bold)
        Me.VizualSmallButton2.Image = Nothing
        Me.VizualSmallButton2.Location = New System.Drawing.Point(194, 234)
        Me.VizualSmallButton2.Name = "VizualSmallButton2"
        Me.VizualSmallButton2.NoRounding = False
        Me.VizualSmallButton2.Size = New System.Drawing.Size(95, 30)
        Me.VizualSmallButton2.TabIndex = 4
        Me.VizualSmallButton2.Text = "Add Process"
        Me.VizualSmallButton2.Transparent = False
        '
        'VizualSmallButton3
        '
        Me.VizualSmallButton3.Colors = New quickShortcut.Bloom(-1) {}
        Me.VizualSmallButton3.Cursor = System.Windows.Forms.Cursors.Hand
        Me.VizualSmallButton3.Customization = ""
        Me.VizualSmallButton3.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Bold)
        Me.VizualSmallButton3.Image = Nothing
        Me.VizualSmallButton3.Location = New System.Drawing.Point(12, 234)
        Me.VizualSmallButton3.Name = "VizualSmallButton3"
        Me.VizualSmallButton3.NoRounding = False
        Me.VizualSmallButton3.Size = New System.Drawing.Size(60, 30)
        Me.VizualSmallButton3.TabIndex = 5
        Me.VizualSmallButton3.Text = "Clear"
        Me.VizualSmallButton3.Transparent = False
        '
        'safeList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(367, 276)
        Me.Controls.Add(Me.VizualSmallButton3)
        Me.Controls.Add(Me.VizualSmallButton2)
        Me.Controls.Add(Me.VizualSmallButton1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.ListBox1)
        Me.Controls.Add(Me.Label1)
        Me.Name = "safeList"
        Me.Text = "safeList"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents VizualSmallButton1 As quickShortcut.VizualSmallButton
    Friend WithEvents VizualSmallButton2 As quickShortcut.VizualSmallButton
    Friend WithEvents VizualSmallButton3 As quickShortcut.VizualSmallButton
End Class
