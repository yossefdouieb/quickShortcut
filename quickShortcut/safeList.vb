﻿Public Class safeList

    Dim separator As String = "&"
    Dim allData As String = ""
    Dim dataSplited As Array

    Private Sub ListBox1_MouseDoubleClick(sender As System.Object, e As System.Windows.Forms.MouseEventArgs) Handles ListBox1.MouseDoubleClick
        'double-click to remove a program from the list
        Try
            ListBox1.Items.RemoveAt(ListBox1.SelectedIndex)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub VizualSmallButton2_Click(sender As System.Object, e As System.EventArgs) Handles VizualSmallButton2.Click
        'show a simple process dialog 
        Process_Dialog.Show()
    End Sub

    Private Sub VizualSmallButton3_Click(sender As System.Object, e As System.EventArgs) Handles VizualSmallButton3.Click
        ListBox1.Items.Clear()
    End Sub

    Private Sub VizualSmallButton1_Click(sender As System.Object, e As System.EventArgs) Handles VizualSmallButton1.Click

        'Remove Empty Cells before saving
        For i = 0 To ListBox1.Items.Count - 1
            'MsgBox(item.ToString)
            Try
                If ListBox1.Items(i).ToString = "" Then
                    ListBox1.Items.RemoveAt(i)
                End If
            Catch ex As Exception

            End Try
        Next

        allData = ""

        'save to app settings, separated with an "&"
        For x = 0 To ListBox1.Items.Count() - 1
            allData += ListBox1.Items(x) + separator
        Next

        My.Settings.safeList = allData
        My.Settings.Save()
        My.Settings.Reload()
        My.Settings.Upgrade()
        MsgBox("Successfully Saved!", MsgBoxStyle.Information)

        'Update Log TextBox
        main.logBox.Text += vbNewLine + "Safe Programs Settings Updated..."
        main.logBox.Refresh()
    End Sub

    Private Sub safeList_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        'filling the listbox with the app settings
        dataSplited = My.Settings.safeList.Split(separator)

        For x = 0 To dataSplited.Length - 1
            ListBox1.Items.Add(dataSplited(x))
        Next
    End Sub

    Private Sub safeList_FormClosing(sender As System.Object, e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        Settings.Show()
    End Sub
End Class