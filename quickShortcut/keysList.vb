﻿Public Class keysList

    Dim dataSplited As Array
    Dim separator As String = "&"
    Dim canSave As Boolean = True

    Private Sub Settings_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        dataSplited = My.Settings.keysData.Split(separator)

        'reading settings and updating the dataGridView
        For x = 0 To dataSplited.Length - 1 Step 2
            If Not x >= dataSplited.Length - 1 Then
                If dataSplited.Length > 1 Then

                    Dim row As String() = {dataSplited(x), dataSplited(x + 1)}
                    DataGridView1.Rows.Add(row)
                End If
            Else
                Exit For
            End If
        Next

    End Sub

    Private Sub VizualSmallButton1_Click(sender As System.Object, e As System.EventArgs) Handles VizualSmallButton1.Click
        Dim allData = ""

        'Deselect The Editing Cell In The DataGrid
        'DataGridView1.Update()
        DataGridView1.ClearSelection()
        DataGridView1.EndEdit()

        'if there is an empty cell - we cant save the settings
        canSave = True
        For Each row As DataGridViewRow In DataGridView1.Rows
            If Not row.IsNewRow Then
                DataGridView1.Refresh()
                If row.Cells(0).Value = "" Or row.Cells(1).Value = "" Then
                    canSave = False
                End If
            End If
        Next

        If canSave Then
            'Save All The DataGrid Data In The App Settings
            For Each row As DataGridViewRow In DataGridView1.Rows
                If Not row.IsNewRow Then
                    DataGridView1.Refresh()
                    allData += row.Cells(0).Value + separator + row.Cells(1).Value + separator
                End If
            Next

            My.Settings.keysData = allData

            'save the settings, refresh and upgrade (to avoid a restart of the app to read the new settings)
            My.Settings.Save()
            My.Settings.Reload()
            My.Settings.Upgrade()
            MsgBox("Successfully Saved!", MsgBoxStyle.Information)

            'Update Log TextBox
            main.logBox.Text += vbNewLine + "Key-Words Settings Updated..."
            main.logBox.Refresh()

        Else
            MsgBox("You Have Empty Cells!", MsgBoxStyle.Exclamation)
        End If
    End Sub

    Private Sub VizualSmallButton2_Click(sender As System.Object, e As System.EventArgs) Handles VizualSmallButton2.Click
        DataGridView1.Rows.Clear()
    End Sub

    Private Sub DataGridView1_CellEndEdit(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellEndEdit
        'if the user left a empty cell, notify him, and color the empty cell with the RED color
        If String.IsNullOrEmpty(DataGridView1.CurrentCell.Value) OrElse DataGridView1.CurrentCell.Value.Trim().Length = 0 Then
            canSave = False
            DataGridView1.CurrentCell.Style.BackColor = Color.Red
            MsgBox("Cell Cant Be Empty...")
        Else
            DataGridView1.CurrentCell.Style.BackColor = Color.White
        End If

        If DataGridView1.CurrentCell.Value = "CODE" Then
            codePad.Show()
        End If

    End Sub

    Private Sub keysList_FormClosing(sender As System.Object, e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        Settings.Show()
    End Sub
End Class