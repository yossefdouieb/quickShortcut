﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class codePad
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.RichTextBox1 = New System.Windows.Forms.RichTextBox()
        Me.VizualSmallButton1 = New quickShortcut.VizualSmallButton()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.VizualSmallButton2 = New quickShortcut.VizualSmallButton()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.VizualSmallButton3 = New quickShortcut.VizualSmallButton()
        Me.txtOutput = New System.Windows.Forms.TextBox()
        Me.VizualCombo1 = New quickShortcut.VizualCombo()
        Me.SuspendLayout()
        '
        'RichTextBox1
        '
        Me.RichTextBox1.Font = New System.Drawing.Font("Arial Nova Cond", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(186, Byte))
        Me.RichTextBox1.Location = New System.Drawing.Point(12, 78)
        Me.RichTextBox1.Name = "RichTextBox1"
        Me.RichTextBox1.Size = New System.Drawing.Size(381, 274)
        Me.RichTextBox1.TabIndex = 0
        Me.RichTextBox1.Text = ""
        '
        'VizualSmallButton1
        '
        Me.VizualSmallButton1.Colors = New quickShortcut.Bloom(-1) {}
        Me.VizualSmallButton1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.VizualSmallButton1.Customization = ""
        Me.VizualSmallButton1.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Bold)
        Me.VizualSmallButton1.Image = Nothing
        Me.VizualSmallButton1.Location = New System.Drawing.Point(590, 358)
        Me.VizualSmallButton1.Name = "VizualSmallButton1"
        Me.VizualSmallButton1.NoRounding = False
        Me.VizualSmallButton1.Size = New System.Drawing.Size(46, 30)
        Me.VizualSmallButton1.TabIndex = 1
        Me.VizualSmallButton1.Text = "OK"
        Me.VizualSmallButton1.Transparent = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(97, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Choose Language:"
        Me.Label1.Visible = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 62)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(34, 13)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Input:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(397, 62)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(42, 13)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Output:"
        '
        'VizualSmallButton2
        '
        Me.VizualSmallButton2.Colors = New quickShortcut.Bloom(-1) {}
        Me.VizualSmallButton2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.VizualSmallButton2.Customization = ""
        Me.VizualSmallButton2.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Bold)
        Me.VizualSmallButton2.Image = Nothing
        Me.VizualSmallButton2.Location = New System.Drawing.Point(347, 358)
        Me.VizualSmallButton2.Name = "VizualSmallButton2"
        Me.VizualSmallButton2.NoRounding = False
        Me.VizualSmallButton2.Size = New System.Drawing.Size(46, 30)
        Me.VizualSmallButton2.TabIndex = 8
        Me.VizualSmallButton2.Text = "Run"
        Me.VizualSmallButton2.Transparent = False
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'VizualSmallButton3
        '
        Me.VizualSmallButton3.Colors = New quickShortcut.Bloom(-1) {}
        Me.VizualSmallButton3.Cursor = System.Windows.Forms.Cursors.Hand
        Me.VizualSmallButton3.Customization = ""
        Me.VizualSmallButton3.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Bold)
        Me.VizualSmallButton3.Image = Nothing
        Me.VizualSmallButton3.Location = New System.Drawing.Point(12, 358)
        Me.VizualSmallButton3.Name = "VizualSmallButton3"
        Me.VizualSmallButton3.NoRounding = False
        Me.VizualSmallButton3.Size = New System.Drawing.Size(77, 30)
        Me.VizualSmallButton3.TabIndex = 9
        Me.VizualSmallButton3.Text = "Open File"
        Me.VizualSmallButton3.Transparent = False
        '
        'txtOutput
        '
        Me.txtOutput.Font = New System.Drawing.Font("Arial Nova Cond", 11.25!)
        Me.txtOutput.Location = New System.Drawing.Point(400, 80)
        Me.txtOutput.Multiline = True
        Me.txtOutput.Name = "txtOutput"
        Me.txtOutput.Size = New System.Drawing.Size(236, 272)
        Me.txtOutput.TabIndex = 10
        '
        'VizualCombo1
        '
        Me.VizualCombo1.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.VizualCombo1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.VizualCombo1.FormattingEnabled = True
        Me.VizualCombo1.ItemHeight = 20
        Me.VizualCombo1.Items.AddRange(New Object() {"VB.Net"})
        Me.VizualCombo1.Location = New System.Drawing.Point(15, 25)
        Me.VizualCombo1.Name = "VizualCombo1"
        Me.VizualCombo1.Size = New System.Drawing.Size(121, 26)
        Me.VizualCombo1.TabIndex = 4
        Me.VizualCombo1.Visible = False
        '
        'codePad
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(648, 400)
        Me.Controls.Add(Me.txtOutput)
        Me.Controls.Add(Me.VizualSmallButton3)
        Me.Controls.Add(Me.VizualSmallButton2)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.VizualCombo1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.VizualSmallButton1)
        Me.Controls.Add(Me.RichTextBox1)
        Me.Name = "codePad"
        Me.Text = "codePad"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents RichTextBox1 As System.Windows.Forms.RichTextBox
    Friend WithEvents VizualSmallButton1 As quickShortcut.VizualSmallButton
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents VizualCombo1 As quickShortcut.VizualCombo
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents VizualSmallButton2 As quickShortcut.VizualSmallButton
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents VizualSmallButton3 As quickShortcut.VizualSmallButton
    Friend WithEvents txtOutput As System.Windows.Forms.TextBox
End Class
